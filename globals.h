//The dimensions of the level
const int LEVEL_WIDTH = 800;
const int LEVEL_HEIGHT = 600;

//Screen dimension constants
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
