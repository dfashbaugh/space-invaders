#include "GameGraphics.h"
#include "LTexture.h"
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "globals.h"
#include "PlayerLaser.h"
#include "EnemyShip.h"
#include "PlayerShip.h"
#include <vector>



GameGraphics::GameGraphics()
    : mPlayerShipTexture()
    , mPlayerShip(SCREEN_WIDTH/2, SCREEN_HEIGHT - (SCREEN_HEIGHT/16) )
{
    //ctor
}

GameGraphics::~GameGraphics()
{
    //dtor
}

void GameGraphics::fillEnemyShipVector()
{
	mEnemies.push_back(new EnemyShip(100, 200) );
	mEnemies.push_back(new EnemyShip(400, 300) );
	mEnemies.push_back(new EnemyShip(604, 500) );
}

void GameGraphics::RunLoop()
{
	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;

	bool fireNewLaser = false;

    fillEnemyShipVector();

	//While application is running
	while( !quit )
	{
		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 )
		{

			//User requests quit
			if( e.type == SDL_QUIT)
			{
				quit = true;
			}

			mPlayerShip.handleEvent( e );

            if( e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_d && e.key.repeat == 0)
            {
                fireNewLaser = true;
            }
		}

        // Perform all movements and fire lasers
        performOnScreenMovements(fireNewLaser);

        // Check collisions between enemies and ball lasers
        checkAllCollisions();

        // Render all objects
		renderAll();
	}
}

// Move the lasers, enemies, and lasers
void GameGraphics::performOnScreenMovements(bool &fireLaser)
{
    // Handle player ship movement
    mPlayerShip.handleMovement();
    mPlayerShip.move();

    // Move all of the enemies
    for(int i = 0; i < (int)mEnemies.size(); i++)
    {
        mEnemies[i]->Move();
    }

    // Fire laser if triggered
    if(fireLaser)
    {
        fireLaser = false;

        mLasers.push_back(new PlayerLaser(mPlayerShip.getPosX() + (mPlayerShip.getWidth()/2), mPlayerShip.getPosY() ) );
    }

    // Move all of the lasers
    for(int i = 0; i < (int)mLasers.size(); i++)
    {
        mLasers[i]->Move();
    }
}

// Render all graphics
void GameGraphics::renderAll()
{
    //Clear screen
    SDL_SetRenderDrawColor( mRenderer, 0x00, 0x00, 0x00, 0x00 );
    SDL_RenderClear( mRenderer );

    // RENDER EVERYTHING

    for(int i = 0; i < (int)mEnemies.size(); i++)
    {
        mEnemies[i]->Render( mRenderer);
    }

    mPlayerShip.render(mRenderer, mPlayerShipTexture);

    for(int i = 0; i < (int)mLasers.size(); i++)
    {
        // Destroy old lasers
        if(mLasers[i]->getDestroy())
        {
            cleanUpOldLasers(i);
        }
        else
        {
            mLasers[i]->Render( mRenderer );
        }
    }

    //Update screen
    SDL_RenderPresent( mRenderer );
}

bool GameGraphics::InitializeWindow()
{
//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		mWindow = SDL_CreateWindow( "Space Evaders", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( mWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			mRenderer = SDL_CreateRenderer( mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if( mRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( mRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

// Load Textures
bool GameGraphics::LoadMedia()
{
	//Loading success flag
	bool success = true;

	//Load player ship texture
	if( !mPlayerShipTexture.loadFromFile(mRenderer, "PlayerShip.bmp" ) )
	{
		printf( "Failed to load ship texture!\n" );
		success = false;
	}

	return success;
}

// Shutdown the graphics
void GameGraphics::ShutdownWindow()
{
	//Destroy window
	SDL_DestroyRenderer( mRenderer );
	SDL_DestroyWindow( mWindow );
	mWindow = NULL;
	mRenderer = NULL;

	mPlayerShipTexture.free();

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

// Check for collisions between lasers and enemies.
void GameGraphics::checkAllCollisions()
{
    for(int i = 0; i < (int)mLasers.size(); i++)
    {
        for(int j = 0; j < (int)mEnemies.size(); j++)
        {
            if( checkCollision(mLasers[i]->getHitBox(), mEnemies[j]->getHitBox() ) )
            {
                if(i == (int)mLasers.size() - 1)
                {
                    mLasers.pop_back();
                }
                else
                {
                    // Swap and pop the bad laser
                    PlayerLaser* curLaser = mLasers[mLasers.size()-1];
                    mLasers[i] = curLaser;
                    mLasers.pop_back();
                }

                if(j == (int)mEnemies.size() - 1)
                {
                    mEnemies.pop_back();
                }
                else
                {
                    // Swap and pop the bad laser
                    EnemyShip* curShip = mEnemies[mEnemies.size()-1];
                    mEnemies[j] = curShip;
                    mEnemies.pop_back();
                }
            }
        }
    }
}

// Check hit box collisions from SDL rectangles
bool GameGraphics::checkCollision(SDL_Rect rect1, SDL_Rect rect2)
{
        //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = rect1.x;
    rightA = rect1.x + rect1.w;
    topA = rect1.y;
    bottomA = rect1.y + rect1.h;

    //Calculate the sides of rect B
    leftB = rect2.x;
    rightB = rect2.x + rect2.w;
    topB = rect2.y;
    bottomB = rect2.y + rect2.h;

    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

// Clean up the lasers that have escaped the screen
void GameGraphics::cleanUpOldLasers(int index)
{
    if(index == (int)mLasers.size() - 1)
    {
        mLasers.pop_back();
    }
    else
    {
        // Swap and pop the bad laser
        PlayerLaser* curLaser = mLasers[mLasers.size()-1];
        mLasers[index] = curLaser;
        mLasers.pop_back();
    }
}
