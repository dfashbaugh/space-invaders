#include "EnemyShip.h"
#include <stdio.h>

EnemyShip::EnemyShip(int posX, int posY)
{
    //ctor

    mHitBox.h = 20;
    mHitBox.w = 20;
    mHitBox.x = posX;
    mHitBox.y = posY;

    mPosX = posX;
    mPosY = posY;

    mVelX = 0;
    mVelY = 0;
}

EnemyShip::~EnemyShip()
{
    //dtor
}

void EnemyShip::Move()
{
    mPosX += mVelX;
    mPosY += mVelY;

    mHitBox.x = mPosX;
    mHitBox.y = mPosY;
}

void EnemyShip::Render(SDL_Renderer* renderer)
{
    mHitBox.x = mPosX;
    mHitBox.y = mPosY;
    SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0x00, 0xFF);
    SDL_RenderFillRect( renderer, &mHitBox);
}
