#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "LTexture.h"
#include "PlayerLaser.h"
#include "EnemyShip.h"
#include "PlayerShip.h"

#ifndef GAMEGRAPHICS_H
#define GAMEGRAPHICS_H

class GameGraphics
{
    public:
        GameGraphics();
        virtual ~GameGraphics();

        bool InitializeWindow();
        bool LoadMedia();
        void ShutdownWindow();
        void RunLoop();

    protected:

        LTexture mPlayerShipTexture;

        void checkAllCollisions();
        bool checkCollision(SDL_Rect rect1, SDL_Rect rect2);
        void fillEnemyShipVector();
        void cleanUpOldLasers(int index);
        void performOnScreenMovements(bool &fireLaser);
        void renderAll();

        SDL_Window* mWindow;
        SDL_Renderer* mRenderer;

        PlayerShip                  mPlayerShip;
        std::vector <PlayerLaser*>  mLasers;
        std::vector <EnemyShip*>    mEnemies;

    private:
};

#endif // GAMEGRAPHICS_H
