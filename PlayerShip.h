#include <SDL.h>
#include "LTexture.h"

#ifndef PLAYERSHIP_H
#define PLAYERSHIP_H

class PlayerShip
{
    public:
        //The dimensions of the dot
		static const int SHIP_WIDTH = 37;
		static const int SHIP_HEIGHT = 25;

		//Maximum axis velocity of the dot
		static const int SHIP_VEL = 5;

        PlayerShip(int startPosX, int startPosY);
        ~PlayerShip();

        //Takes key presses and adjusts the dot's velocity
		void handleEvent( SDL_Event& e);

		//Moves the dot
		void move();

		//Shows the dot on the screen relative to the camera
		void render( SDL_Renderer* renderer, LTexture& theTexture );

		//Position accessors
		int getPosX();
		int getPosY();

		int getWidth()  {return SHIP_WIDTH; };

		void handleMovement();

    protected:
    private:

        		//The X and Y offsets of the dot
		int mPosX, mPosY;

		//The velocity of the dot
		int mVelX, mVelY;

		bool mKeyDownRight;
		bool mKeyDownLeft;

		int mAcceleration;

		int mLeftRightAcceleration;
};

#endif // PLAYERSHIP_H
