#include "PlayerLaser.h"

PlayerLaser::PlayerLaser(int posX, int posY)
{
    //ctor

    mLaserShape.h = 20;
    mLaserShape.w = 5;
    mLaserShape.x = posX;
    mLaserShape.y = posY;

    mPosX = posX;
    mPosY = posY;

    mVelX = 0;
    mVelY = -20;
}

PlayerLaser::~PlayerLaser()
{
    //dtor
}

void PlayerLaser::Move()
{
    mPosX += mVelX;
    mPosY += mVelY;

    mLaserShape.x = mPosX;
    mLaserShape.y = mPosY;
}

void PlayerLaser::Render(SDL_Renderer* renderer)
{
    mLaserShape.x = mPosX;
    mLaserShape.y = mPosY;
    SDL_SetRenderDrawColor( renderer, 0xFF, 0x00, 0x00, 0xFF);
    SDL_RenderFillRect( renderer, &mLaserShape);
}

bool PlayerLaser::getDestroy()
{
    if(mPosY < 0)
    {
        return true;
    }

    return false;
}

SDL_Rect PlayerLaser::getHitBox()
{
    return mLaserShape;
}
