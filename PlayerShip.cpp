#include "PlayerShip.h"
#include "globals.h"

PlayerShip::PlayerShip(int startPosX, int startPosY)
{
    mPosX = startPosX;
    mPosY = startPosY;

    mVelX = 0;
    mVelY = 0;

    mKeyDownRight = false;
    mKeyDownLeft = false;

    mAcceleration = 1;

    mLeftRightAcceleration = 1;
}

PlayerShip::~PlayerShip()
{
    //dtor
}

void PlayerShip::handleEvent( SDL_Event& e)
{
    //If a key was pressed
	if( e.type == SDL_KEYDOWN && e.key.repeat == 0 )
    {
        //Adjust the velocity
        switch( e.key.keysym.sym )
        {
            case SDLK_LEFT: mKeyDownLeft = true; break;
            case SDLK_RIGHT: mKeyDownRight = true; break;
        }
    }
    //If a key was released
    else if( e.type == SDL_KEYUP && e.key.repeat == 0 )
    {
        //Adjust the velocity
        switch( e.key.keysym.sym )
        {
            case SDLK_LEFT: mKeyDownLeft = false; break;
            case SDLK_RIGHT: mKeyDownRight = false; break;
        }
    }
}

void PlayerShip::move()
{
    //Move the dot left or right
    mPosX += mVelX;

    //If the dot went too far to the left or right
    if( ( mPosX < 0 ) )
    {
        //Move back
        mPosX = 0;
    }
    else if(( mPosX + SHIP_WIDTH > LEVEL_WIDTH ) )
    {
        mPosX = LEVEL_WIDTH - SHIP_WIDTH;
    }
}

void PlayerShip::render( SDL_Renderer* renderer, LTexture& theTexture)
{
    //Show the dot relative to the camera
	theTexture.render( renderer, mPosX , mPosY  );
}

int PlayerShip::getPosX()
{
	return mPosX;
}

int PlayerShip::getPosY()
{
	return mPosY;
}

void PlayerShip::handleMovement( )
{
    if(mKeyDownLeft)
    {
        if(mVelX > -1*SHIP_VEL)
        {
            mVelX -= mLeftRightAcceleration;
        }
    }
    else if(mKeyDownRight)
    {
        if(mVelX < SHIP_VEL)
        {
            mVelX += mLeftRightAcceleration;
        }
    }
    else
    {
        if(mVelX > 0)
        {
            mVelX -= mLeftRightAcceleration;
        }
        else if(mVelX < 0 )
        {
            mVelX += mLeftRightAcceleration;
        }
    }

}
