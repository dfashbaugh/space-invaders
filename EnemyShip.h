#include <SDL.h>

#ifndef ENEMYSHIP_H
#define ENEMYSHIP_H


class EnemyShip
{
    public:
        EnemyShip(int posX, int posY);
        ~EnemyShip();

        void        Move        ();
        void        Render      (SDL_Renderer* renderer);

        SDL_Rect    getHitBox   ()      {return mHitBox; };

    protected:

        int mPosX;
        int mPosY;

        int mVelX;
        int mVelY;

        SDL_Rect    mHitBox;
    private:
};

#endif // ENEMYSHIP_H
